
package ProjectThree;
import java.util.Scanner;
public class LeapYear {
    public static void main(String args[]){
        int year;
        boolean isLeap = false;
        Scanner sc= new Scanner(System.in);
        System.out.println("ENter Your Day To check if it is LeapYear");
        year = sc.nextInt();
        
        if(year%4 == 0)
        {
          if(year%100 == 0)
          {
              if(year%400 == 0)
              {
                  isLeap = true;
              }
              else
                  isLeap = false;
               }
          else {
              isLeap = true;
          }
        }
        else {
            isLeap = false;
        }
        
        if(isLeap == true){
            System.out.println("Leap Year");
        }
        else{
           System.out.println("Not Leap Year"); 
        }
        
    }
}
